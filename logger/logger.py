from common import Singleton
from enum import Enum


class LogLevel(Enum):
    Critical = 0
    Major = 1
    Minor = 2
    Info = 3
    Debug = 4


class Logger(Singleton):
    def __init__(self, min_level=LogLevel.Debug):
        """
        Initiates a central logging entity
        :param min_level: The lowest level the logger should recognize
        """
        self.min_level = min_level
        self.log = []

    def critical(self, message: str):
        self.write(LogLevel.Critical, message)

    def major(self, message: str):
        self.write(LogLevel.Major, message)

    def minor(self, message: str):
        self.write(LogLevel.Minor, message)

    def info(self, message: str):
        self.write(LogLevel.Info, message)

    def debug(self, message: str):
        self.write(LogLevel.Debug, message)

    def write(self, level: LogLevel, message: str):
        if self.min_level.value <= level.value:
            self.log.append(message)

    def clear(self):
        self.log.clear()







