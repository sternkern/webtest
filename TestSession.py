from enums import TestPriority


class TestSession:
    def __init__(self, name: str, shuffle: bool):
        self.__test_list = []
        self.__priority = TestPriority.High
        self.__background = False
