import wx
from config import Configuration

ID_MAIN_PANEL = 5200
ID_PAUSE = 5500


class TestRunnerFrame(wx.Frame):
    def __init__(self):
        self.__config = Configuration()
        super(TestRunnerFrame, self).__init__(None, title=f'TestRunner server {self.__config.test_runner_version}',
                                              size=(800,600))
        self.__menu_bar = wx.MenuBar()
        self.__status_menu = wx.Menu()
        self.__pause_menu_item = self.__status_menu.Append(ID_PAUSE, 'Pause', 'Pauses the test execution')
        self.__menu_bar.Append(self.__status_menu, '&Status')
        self.SetMenuBar(self.__menu_bar)
        self.panel = MainPanel(self)

    def update_server_state(self, state: str):
        self.panel.server_state_cap.SetLabel(f'Server status: {state}')


class MainPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        self.info = wx.TextCtrl(self, ID_MAIN_PANEL, '', pos=(0,0), size=(800, 450),
                                style=wx.TE_MULTILINE and wx.TE_READONLY)
        self.server_state_cap = wx.StaticText(self, label='Server status: Init', pos=(20, 480))

