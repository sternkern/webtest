import wx

from enums import TestPriority, TestRunnerState
from GUI.TestRunnerGUI import TestRunnerFrame
from logger.logger import Logger
from config import Configuration
from TestQueue import TestQueue


class TestRunner(object):
    """
    Queues test cases for execution, executes them
    """
    def __init__(self):
        self.__old_state = self.__state = TestRunnerState.Init
        self.__queue = TestQueue()
        self.__config = Configuration()
        self.__logger = Logger()
        self.__app = wx.App(redirect=False)
        self.__gui = TestRunnerFrame()
        self.__gui.Show()
        self.__app.MainLoop()

    def enqueue(self, testcase, priority=TestPriority.Low, background=True):
        """
        Puts a testcase into the execution queue with the specified priority and background state.
        :param testcase: A string denoting the testcase to be executed
        :param priority: Integer value
        :param background: If it is true other non-background tests will be prioritized over this one
        :return:
        """
        TestQueue.add_test(testcase, priority, background)

    def shutdown(self) -> None:
        """
        Stops all executions and closes all connections, stops the TestRunner
        :return:
        """
        pass

    def run(self) -> None:
        """
        The main loop of the TestRunner class
        :return:
        """
        Logger.info('Starting main loop')
        while self.__state != TestRunnerState.Stopped:
            self.__queue.process()

    def change_state(self, new_state: TestRunnerState):
        """
        Changes the state of the TestRunner, saves the old state
        :param new_state: The required new state
        :return:
        """
        self.__old_state = self.__state
        self.__state = new_state
        self.__gui.update_server_state(self.__state.name)


if __name__ == "__main__":
    runner = TestRunner()
