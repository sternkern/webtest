from enum import Enum


class TestRunnerState(Enum):
    Init = 0
    Idle = 1
    Running = 2
    ShuttingDown = 3
    Stopped = 4


class TestPriority(Enum):
    High = 0
    Medium = 1
    Low = 2


class TestStatus(Enum):
    REQUESTED = 0
    LOADING = 1
    QUEUED = 2
    RUNNING = 3
    PAUSED = 4
    FINISHED = 5
