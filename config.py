import json

from common import Singleton
from logger.logger import LogLevel


class Configuration(Singleton):
    def __init__(self):
        self.__data = {
            "min_loglevel": LogLevel.Debug,
            'test_runner_version': '0.1'
        }
        for key, value in self.__data.items():
            setattr(Configuration, key, value)

    def save(self):
        pass

    def load(self):
        pass
