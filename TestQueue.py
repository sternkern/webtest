from datetime import time
from enums import TestStatus
from logger.logger import Logger
from typing import Dict, Union


class TestQueue:
    def __init__(self):
        self.__logger = Logger()
        self.__queue = []
        self.__actual_test: Union[Dict, None]= None

    def add_test(self, test_name, priority, background) -> None:
        """
        Inserts a test into the queue with the appropriate flags
        :param test_name: a unique string identifier which denotes a test in the test folder
        :param priority: The system ranks the enqueued tests based on this attribute
        :param background: The test will be paused if a foreground test arrives
        :return:
        """
        self.__queue.append({'name': test_name,
                             'priority': priority,
                             'background': background,
                             'enqueue time': time(),
                             'status': TestStatus.REQUESTED})

    def process(self) -> None:
        """
        If the queue is not empty it makes sure there is a test running, also it makes sure the tests in the queue
        are loaded
        """
        if self.__actual_test is None:
            if not self.__queue:
                return
            else:
                self.__actual_test = self.__queue.pop()
                Logger().info(f"Running test {self.__actual_test['name']}")
        else:
            return

    def execute(self):
        pass

